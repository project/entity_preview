<?php

namespace Drupal\entity_preview\Access;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access to entity previews.
 *
 * @ingroup preview_access
 */
class EntityPreviewAccessCheck implements AccessInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a EntityCreateAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Checks access to the entity preview page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\Core\Entity\EntityInterface $entity_preview
   *   The entity that is being previewed.
   *
   * @return string
   *   A \Drupal\Core\Access\AccessInterface constant value.
   */
  public function access(AccountInterface $account, EntityInterface $entity_preview) {
    if ($entity_preview->isNew()) {
      $access_controller = $this->entityTypeManager->getAccessControlHandler($entity_preview->getEntityTypeId());
      return $access_controller->createAccess($entity_preview->bundle(), $account, [], TRUE);
    }
    else {
      return $entity_preview->access('update', $account, TRUE);
    }
  }

}
