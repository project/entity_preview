<?php

namespace Drupal\entity_preview\Controller;

use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to render a single entity in preview.
 */
class EntityPreviewController extends EntityViewController {

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Creates an EntityPreviewController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, EntityRepositoryInterface $entity_repository = NULL) {
    parent::__construct($entity_type_manager, $renderer);
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity_preview, $view_mode_id = 'default', $langcode = NULL) {
    return [
      '#lazy_builder' => [
        'Drupal\entity_preview\BlockPreviewBuilder::lazyBuilder',
        [
          $entity_preview->uuid(),
          $view_mode_id,
        ],
      ],
    ];
  }

  /**
   * The _title_callback for the page that renders a single entity in preview.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity_preview
   *   The current entity.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $entity_preview) {
    return $this->entityRepository->getTranslationFromContext($entity_preview)->label();
  }

}
