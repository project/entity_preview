<?php

namespace Drupal\entity_preview;

use Drupal\block\BlockViewBuilder;

/**
 * Overrides Block view builder.
 */
class BlockPreviewBuilder extends BlockViewBuilder {

  /**
   * {@inheritdoc}
   */
  public static function lazyBuilder($entity_id, $view_mode) {
    $blockStorage = \Drupal::service('entity_type.manager')->getStorage('block');
    $block_instance = $blockStorage->create([
      'id' => $entity_id,
      'plugin' => 'block_content:' . $entity_id,
    ]);

    // Override block content view mode.
    $block_instance->getPlugin()->setConfiguration(['view_mode' => $view_mode]);

    $build = parent::buildPreRenderableBlock($block_instance, \Drupal::service('module_handler'));
    $build['#contextual_links'] = [];
    $build['#cache'] = [];

    return $build;
  }

}
